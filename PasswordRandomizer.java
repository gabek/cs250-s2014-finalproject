/*
 * Gabe Kelly
 * Dr. Jumadinova
 * CS 250
 * April 15th, 2014
 * This randomizes the original password
 * and returns a new one.
 * There are several randomizations. 
 * One of which being adding a new character,
 */
public class PasswordRandomizer
{
    private static String list = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890!`~@#$%^&*()_-+=\\|}{[]:;\"'<,>.?/ ";
    
    public PasswordRandomizer()
    {
    }

    /**
     * passAdditon()
     * @Returns a new String Password longer than the original
     * @Params a String for a password
     **/

    public String passAddition(String original)
    {
        int place = (int) (Math.random() * list.length());

        String password = original + list.charAt(place);

        return password;
    }

    /**
     * passRandom()
     * @Returns a new String password with a new character
     * @Params a string for a password
     **/

    public String passRandom(String original)
    {
        int listPlace = (int) (Math.random() * list.length());

        int origPlace = (int) (Math.random() * original.length());

        char[] copy = original.toCharArray();

        copy[origPlace] = list.charAt(listPlace);

        String password = "";

        for(int i = 0; i < copy.length; i++)
        {
            password = password + copy[i];
        }

        return password;
    }

    /**
     * passUpper()
     * @Returns a new String password with a new upper
     * @Params a string for a password
     **/

    public String passUpper(String original)
    {
        String upper = list.substring(0, 26);//

        int listPlace = (int) (Math.random() * upper.length());

        int origPlace = (int) (Math.random() * original.length());

        char[] copy = original.toCharArray();

        copy[origPlace] = upper.charAt(listPlace);

        String password = "";

        for(int i = 0; i < copy.length; i++)
        {
            password = password + copy[i];
        }

        return password;
    }


    /**
     * passDigit()
     * @Returns a new String password with a character replaced by a digit
     * @Params a string for a password
     **/

    public String passDigit(String original)
    {
        String digit = list.substring(52, 62);

        int listPlace = (int) (Math.random() * digit.length());

        int origPlace = (int) (Math.random() * original.length());

        char[] copy = original.toCharArray();

        copy[origPlace] = digit.charAt(listPlace);

        String password = "";

        for(int i = 0; i < copy.length; i++)
        {
            password = password + copy[i];
        }

        return password;
    }

    /**
     * passSpecial()
     * @Returns a new String password with a character replaced by a special char.
     * @Params a string for a password
     **/

    public String passSpecial(String original)
    {
        String special = list.substring(62);

        int listPlace = (int) (Math.random() * special.length());

        int origPlace = (int) (Math.random() * original.length());

        char[] copy = original.toCharArray();

        copy[origPlace] = special.charAt(listPlace);

        String password = "";

        for(int i = 0; i < copy.length; i++)
        {
            password = password + copy[i];
        }

        return password;
    }

    public String passNoSpecial(String original)
    {
        String noSpecial = list.substring(0, 62);

        int listPlace = (int) (Math.random() * noSpecial.length());

        int origPlace = (int) (Math.random() * original.length());

        char[] copy = original.toCharArray();

        copy[origPlace] = noSpecial.charAt(listPlace);

        String password = "";

        for(int i = 0; i < copy.length; i++)
        {
            password = password + copy[i];
        }

        return password;
    }

    public String addNoSpecial(String original)
    {
        String noSpecial = list.substring(0, 62);
        
        int place = (int) (Math.random() * noSpecial.length());

        String password = original + noSpecial.charAt(place);

        return password;
    }


}

