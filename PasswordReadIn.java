/*
 * Gabe Kelly
 * Dr. Jumadinova
 * CS 250
 * March 24th, 2014
 * This is the code to read in a password
 * that a user wants to improve.
 * May include more features later
 */
import java.util.Scanner;

public class PasswordReadIn
{
    public PasswordReadIn()//may add params as command line arguments (ie. restrictions like must be
        //eight characters long
    {
    }

    public String createPass(String param)
    {
        String password;

        boolean satisfied;

        boolean[] all = new boolean[3];//for the all command

        do
        {
        satisfied = true;

        System.out.println("Please Make a password");
        //this is where restrictions go

        Scanner scan = new Scanner(System.in);

        password = scan.nextLine();

        switch(param)
        {
            case "-8": satisfied = eightChars(password);
                       break;

            case "-s": satisfied = noSpecial(password);
                       break;

            case "-u": satisfied = upperAndDigit(password);
                       break;

            case "-all": all[0] = eightChars(password);
                        all[1] = noSpecial(password);
                        all[2] = upperAndDigit(password);
                        for (int i = 0; i < all.length; i++)
                        {
                            if (all[i] == false)//if any one of all equals false satisfied is false
                                satisfied = false;
                        }
                        break;

        }
        }
        while(satisfied == false);
        
        return password;
    }

    public static boolean eightChars(String password)
    {
        boolean satisfied = true;
        if(password.length() < 8)
        {
            System.out.println("The password must be eight characters long");
            satisfied = false;
           
        }
        
        return satisfied;
    }

    public static boolean noSpecial(String password)
    {
        boolean satisfied = true;
        String special = "!`~@#$%^&*()_-+=\\|}{[]:;\"'<,>.?/ ";
        //System.out.println(special.length());
        for(int i =0; i < special.length(); i++)
        {
            if(password.indexOf(special.charAt(i)) != -1)
            {
                System.out.println("No special characters like !, @, etc.");
                satisfied = false;
                break;
            }
        }

        return satisfied;
    }

    public static boolean upperAndDigit(String password)
    {
        String upperDigit = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        
        int has = 0;
        
        boolean satisfied = true;
        for(int i = 0; i < upperDigit.length(); i++)
            {
                has = password.indexOf(upperDigit.charAt(i));

                if(has != -1)
                {
                    break;
                }
            }
        if (has == -1)
        {
            System.out.println("Needs and uppercase letter or a number");
            satisfied = false;
        }
        return satisfied;
    }
}

