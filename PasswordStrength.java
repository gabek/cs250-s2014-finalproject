/*
 * Gabe Kelly
 * Dr. Jumadinova
 * CS 250
 * April 10th, 2014
 * This program takes in your password 
 * and returns how strong it is
 * Algorithm based on 
 * "How Does Your Password Measure Up?
 * The Effect of Strength Meters at Password Creation".
 * Java implementation is my own however.
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class PasswordStrength
{
    public PasswordStrength()
    {
    }
    
    /**
     * strengthAnalysis
     * Params: a string containing a potential password
     * Return: A Int that is the passwords strength
     **/
    public int strengthAnalysis(String password) throws FileNotFoundException
    {
        int size = password.length();

        int strength = 0;

        ArrayList<Character> uppercase = new ArrayList<Character>();//a list of all the uppercase char in password

        ArrayList<Character> specialChar = new ArrayList<Character>();//a list of all the special char in password

        ArrayList<Character> digitChar = new ArrayList<Character>();//a list of all the digits in password

        int upperUtil = 17;//the strength of each unique uppercase suffers diminishing marginal returns

        int digitUtil = 17;//the same thing happens with digits.

        int specialUtil = 17;//the same thing happens with special characters.
        
        char[] uniqueUpper = new char[26];//list of unique uppercase characters

        char[] uniqueSpecial = new char[32];
        
        char[] uniqueDigit = new char[10];

        int[] location = new int[100];//stores location of a character in the string

        int count = 0;//tracks where in the array the location is at

        String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";//uppercase

        String digit = "0123456789";//digits

        String special = "!@#$%^&*()_+=-{}[]|\\;':\"<>,.?/~`";//special characters
        
        System.getProperty("user.dir");//filepath to cracklib-words

        char firstChar = password.charAt(0);//gets first character to go to the proper dictonary

        String filename = "";
        
        boolean charSpecial = false;//tests if the first character is a special character

        try{
            for(int i = 0; i < special.length(); i++)
            {
                if (firstChar == special.charAt(i))
                    charSpecial = true;
            }
            
            if(charSpecial)
            {
                filename = "specialcracklib-words";
            }
            else
            {
                filename = firstChar + "cracklib-words";
            }

        
            Scanner scan = new Scanner(new File(filename));
        
        /*
         * cracklib is not my creation
         * Nor do i intend to claim it as my own.
         * It is a free to download unix/linux
         * file of cracked passwords.
         * It falls under opensource and therefore
         * free to use. All credits and otherwise
         * go to the sourceforge.net and the people
         * who made cracklib-words.
         */

            while(scan.hasNextLine())
            {
                if(password.equals(scan.nextLine()))
                    return strength;
            }
        }

        catch(Exception e)
        {
        }

        if(size > 8)//first 8 characters are scored differently
        {
        size = size - 8;

        strength = size * 8;//9th character and beyond are harder to crack therefore eight

        strength = strength + 32;
        }
        else{
        strength = size * 4;//but the first eight are weaker and therefore 4
        }

        String copy = password.toUpperCase();

        //System.out.println(copy);
        for(int i = 0; i < copy.length(); i++)
        {
            if(copy.charAt(i) == password.charAt(i))
            {
                location[count] = i;//gets the location of all non lowercase characters
                //System.out.println(location[count]);//debug
                count++;
            }


        }
        
        for(count = 0; count < location.length; count++)//i really could probably break all of these up into their own methods
        {
            for(int i = 0; i< upper.length(); i++)
            {
                if(upper.charAt(i) == password.charAt(location[count]))//gets the uppercase characters
                {
                    uppercase.add(password.charAt(location[count]));
                    //System.out.println(password.charAt(location[count]));
                    break;
                }
            }
            
            for(int i = 0; i < special.length(); i++)
            {
                if(special.charAt(i) == password.charAt(location[count]))//get the special characters
                {
                    specialChar.add(password.charAt(location[count]));
                    //System.out.println(password.charAt(location[count]));
                    break;
                }
            }

            for(int i = 0; i < digit.length(); i++)
            {
                if(digit.charAt(i) == password.charAt(location[count]))//get the digits
                {
                    digitChar.add(password.charAt(location[count]));
                    //System.out.println(password.charAt(location[count]));
                    break;
                }
            }
        }
            if(copy.equals(password) || uppercase.size() == 0)
            {
                strength = strength - 17;//weaker if it is all lowercase and numbers and such.
            }

            //count = 0;

            for(int i = 0; i < uppercase.size(); i++)
            {
                int result = Arrays.binarySearch(uniqueUpper, (char)uppercase.get(i));

                if(result <= 0)
                {
                    strength = strength + upperUtil;//password gets stronger

                    upperUtil = upperUtil/2;//the strength of each new upper case decreases over time

                    uniqueUpper[i] = uppercase.get(i);//adds new uppercase to list
                }
            }
            
            for(int i = 0; i < specialChar.size(); i++)
            {
                int result = Arrays.binarySearch(uniqueSpecial, (char)specialChar.get(i));

                if(result <= 0)
                {
                    strength = strength + specialUtil;//password gets stronger

                    specialUtil = specialUtil/2;//the strength of each new special character decreases over time

                    uniqueSpecial[i] = specialChar.get(i);//adds new special character to list
                }
            }
            
            for(int i = 0; i < digitChar.size(); i++)
            {
                int result = Arrays.binarySearch(uniqueDigit, (char)digitChar.get(i));

                if(result <= 0)
                {
                    strength = strength + digitUtil;//password gets stronger

                    digitUtil = digitUtil/2;//the strength of each new digit decreases over time

                    uniqueDigit[i] = digitChar.get(i);//adds new digit to list
                }
            }


        return strength;
    }
}
