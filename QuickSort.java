/*
 * This code is once again modified from a previous lab.
 * I thought about making a completely new code for this 
 * but since I had this from both 440 and 250 I might as well use it.
 * There are several new methods for initializing the setup of 
 * the quick sort due to the fact it sorts the passwords by their
 * strength not by string.
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.Iterator;
import java.util.Collection;

public class QuickSort
{
    public static ArrayList<Integer> quickSort(ArrayList<Integer> input)
    {
        int middle = (int) input.size()/2;//middle of arraylist
        int pivot = input.get(middle);//pivot to be used
        input.remove(middle);//and now gotten rid of

        ArrayList<Integer> equivalent = new ArrayList<Integer>();//in case multiple files equals the pivot
        ArrayList<Integer> smaller = new ArrayList<Integer>();//file sizes less than pivot
        ArrayList<Integer> greater = new ArrayList<Integer>();//file sizes greater than pivot

        for(int i = 0; i < input.size(); i++)
        {
            if(input.get(i) <= pivot)
            {
                if(input.get(i) != pivot)
                {
                    smaller.add(input.get(i));//if its less than pivot smaller
                }
                else
                {
                    equivalent.add(input.get(i));//if its equal to pivot

                }
            }
            else
            {
                greater.add(input.get(i));//if its greater than pivot, well greater
            }
        }
        if(smaller.size() > 1)
            smaller = quickSort(smaller);//recursion on smaller if there is more than one

        if(greater.size() > 1)
            greater = quickSort(greater);//recursion on greater if there is more than one

        
        ArrayList<Integer> sorted = new ArrayList<Integer>();//sorted array list

        for(int i = 0; i < smaller.size(); i++)
        {
            sorted.add(smaller.get(i));//adding smaller to sorted list
        }

        sorted.add(pivot);//adding pivot
        
        for(int i = 0; i <equivalent.size(); i++)
        {
            sorted.add(equivalent.get(i));//adding anything equal to the sorted list
        }

        for(int i = 0; i < greater.size(); i++)
        {
            sorted.add(greater.get(i));//adding greater to sorted list
        }

        return sorted;
    }

    public static ArrayList<Integer> valueExtract(HashMap<String, Integer> input)
    {
        Collection<Integer> values = input.values();//get a collection of values

        ArrayList<Integer> list = new ArrayList<Integer>();//arraylist

        Iterator<Integer> iter = values.iterator();//make an iterator to go through the strength of each password

        while(iter.hasNext())//change what how this works.IE switch the fact now i just get the values not the keys
        {
            list.add(iter.next());//iterate through keys
        }
         return list;
    }

    public String[] sortList(HashMap<String, Integer> input)
    {
        ArrayList<Integer> list = valueExtract(input);

        //System.out.println(input.size());

        //System.out.println(list.size());

        String[] passwords = new String[list.size()];//making an array

        ArrayList<Integer> sorted = quickSort(list);//sorted list of strengths

        //System.out.println(sorted.size());

        Set<String> keys = input.keySet();//this is how badly i did not want to sort strings

        Iterator<String> iter = keys.iterator();
        
        int[] location = new int[passwords.length];//location of each strings relative strength in the arraylist

        int count = 0;//a counter
        
        ArrayList<String> passes = new ArrayList<String>();//unsorted arraylist of passwords

        while(iter.hasNext())//change what how this works.IE switch the fact now i just get the values not the keys
        {
            passes.add(iter.next());//iterate through keys
            //System.out.println(passes.get(count));            
        }
        
        

        for(int i = 0; i < list.size(); i++)
        {
            location[i] = sorted.indexOf(input.get(passes.get(i)));//gets the first location of each passwords relative strength
            //debuggSystem.out.println("Location: " + location[i] + ", Password: " + passes.get(i));
        }

        while(count < location.length)
        {
            if(passwords[location[count]] == null)
            {
                passwords[location[count]] = passes.get(count);//the most messed up statment to fill an array. yet necessary
                //System.out.println("Count: " + count + ", Location: " + location[count]);
                //System.out.println(list.size());debugg statments
                count++;//
                //System.out.println("Count: " + count
            }
            else
            {
                location[count]++; //increments location in final array.
            }

            //System.out.println(count);debugg
        }
        return passwords;
    }
}
