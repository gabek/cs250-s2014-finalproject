/*
 * Gabe Kelly
 * Dr. Jumadinova
 * CS 250
 * April 23rd, 2014
 * This is the main method for
 * the password strength application.
 * The purpose of this App is to reveal to people
 * what makes a strong password, and hopeful help
 * in making strong passwords that are easy to
 * remember and safe to use. 
 */

/*The first thing to call is password read in, which
 * may eventually have more methods to it.
 * Then password strength. 
 * After password strength I will store the strength in a int.
 * I will then construct a Hash Map and use
 * password Randomizer until there are 15 passwords stronger than
 * the original password. This will take a while.
 * After that I will parse and Sort the HashMap using quicksort
 * and return the array of passwords ranging from stronger to weaker.
 * Note: I will also include lots of output for users to see.
 */
import java.util.HashMap;

public class strengthApp
{
    public static void main(String[] args)
    {
        PasswordReadIn read = new PasswordReadIn();

        PasswordStrength analyzer = new PasswordStrength();

        QuickSort quick = new QuickSort();

        PasswordRandomizer random = new PasswordRandomizer();
        
        String param = args[0];
        
        int sOriginal = 0;//the originals strength

        int sCopy = 0;//the strength of the copy

        String copy = "";//the new password. Will change to new.

        String[] passlist = new String[15];

        //int count = 0;//debugging

        HashMap<String, Integer> passMap = new HashMap<String, Integer>();//the map of all password copies and their strengths

        String original = read.createPass(param);//the original password

        try
        {
            sOriginal = analyzer.strengthAnalysis(original);
        }

        catch(Exception e)
        {
        }
        
        System.out.println("Your password strength is " + sOriginal);
        
        if(sOriginal > 0 && sOriginal <= 32)
        { 
            System.out.println("This password is fairly weak");
        }
        else
            if(sOriginal > 32 && sOriginal <= 75)
            {
                System.out.println("This password is mediocre");
            }
        else 
            if(sOriginal > 75 && sOriginal < 115)
            {
                System.out.println("This password is pretty strong");
            }
            else
                if(sOriginal > 115)
                { 
                    System.out.println("This password is very strong");
                }
                else
                {
                    System.out.println("This password is broken. Change immeadiatly");
                }

        while(passMap.size() < 15)
        {
            int number = (int) (Math.random() * 5);

            switch(number)
            {
                case 0: if(!param.equals("-s") && !param.equals("-all"))
                            copy = random.passAddition(original);//adds new char
                        else
                            copy = random.addNoSpecial(original);
                        break;


                case 1: if(!param.equals("-s") && !param.equals("-all"))
                            copy = random.passRandom(original);//replaces char
                        else
                            copy = random.passNoSpecial(original);

                        break;

                case 2: copy = random.passUpper(original);//replace w/ uppercase
                        break;

                case 3: copy = random.passDigit(original);//replace w/ digit
                        break;

                case 4: if(!param.equals("-s") && !param.equals("-all"))
                            copy = random.passSpecial(original);//replace w/ special
                        break;
            }
            
            try
            {
                sCopy = analyzer.strengthAnalysis(copy);//how strong the copy is
            }

            catch(Exception e)
            {
            }

            if(sCopy > sOriginal)
            {
                passMap.put(copy, sCopy);//if copy is stronger than original goes into hashmap
            }
            //bug cannot reach 15 because. well thats not how Hashes work. Or HashMaps in java
            //at the very least. One key per hashmap. So replace with something or run copy through.
            //Refit hash map to <string, integer> DO IT QUICKLY!!!!
            //System.out.println(sCopy + ">" + sOriginal);
            
        }

        passlist = quick.sortList(passMap);//sorts the list

        System.out.println("The following passwords are all stronger than " + original + ":");

        for(int i = 0; i < passlist.length; i++)
        {
            System.out.println(passlist[i] + ": " + passMap.get(passlist[i]));//and prints array
        }
    }
}
